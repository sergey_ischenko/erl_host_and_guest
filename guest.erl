-module(guest).

-export([start/1, stop/0, execute/1]).

execute(Host_Node) ->
  io:format("Guest is starting."),
  process:info(),
  io:format("Visiting host...~n", []), {host_process, Host_Node} ! {visit, self()},
  await_for_message().

await_for_message() ->
  receive
    stop ->
      io:format("Guest is being stopped.~n", []),
      exit(normal)
  end,
  io:format("Nothing to do more~n", []).





%%% Controlling from outside

% Run after host_process was started on Host_Node
start(Host_Node) -> 
  register(guest_process, spawn(guest, execute, [Host_Node])).
  
stop() ->
  guest_process ! stop.







