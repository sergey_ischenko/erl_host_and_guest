## Distributed programming on Erlang is awesomely easy!

* Place process.beam and host.beam files into some directory on any computer, suppose comp1. Start host process:

>    user@comp1 > erl -sname host_node
>    (host_node@comp1)1> host:start().

* Place process.beam and host.beam files into some directory on any computer, suppose comp2. Start guest process (telling him name@node of host process):

>    user@comp2 > erl -sname guest_node
>    (guest_node@comp2)1> guest:start('host_node@comp1').

* The guest process will visit host process and will be immediately stopped by him (kill-hello).

* The host process will work (and receive messages from as many guests as you run) until you stop it:

>    (host_node@comp1)1> host:stop().

Look and feel it! :)
