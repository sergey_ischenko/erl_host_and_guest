-module(process).

-export([info/0]).

info() ->
  io:format("PID: ~p, ~p~n", [self(), process_info(self(), registered_name)]).