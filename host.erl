-module(host).

-export([start/0, stop/0, execute/0]).

execute() ->
  io:format("Host is starting."),
  process:info(),
  await_for_message().
  
await_for_message() ->
  receive
    {visit, Guest_PID} ->
      io:format("Hi, guest ~p from node ~p! You have visited me. Now I am going to stop you.~n", [Guest_PID, node(Guest_PID)]),
      Guest_PID ! stop,
      await_for_message();
    stop ->
      io:format("Host is being stopped.~n", []),
      exit(normal)
  end,
  io:format("Nothing to do more~n", []).



%%% Controlling from outside

start() -> 
  register(host_process, spawn(host, execute, [])).
  
stop() ->
  host_process ! stop.








